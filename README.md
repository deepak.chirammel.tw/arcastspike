 ## Running via command-line (cmd Terminal):
 `gradlew run`

 ## Running via Bash Terminal:
 `./gradlew run`


 ## Running from Editor (from IntelliJ):
 Run `src/main/java/com/tw/arcast/server/RxNettyHttpServer.java` for UnSecured connection.

 Run `src/main/java/com/tw/arcast/server/RxNettyHttpsServer.java` for Secured connection using the `unsafeSecure()` approach.
 
 Run `src/main/java/com/tw/arcast/secure/RxNettyHttpsSecureSignedSslCodecServer.java` for Secured connection using the `secure(SslCodec)` approach using the `certificate.crt` and `privateKey.key` from `resources`.
 
 Run `src/main/java/com/tw/arcast/secure/RxNettyHttpsSecureSslCodecServer.java` for Secured connection using the `secure(SslCodec)` approach using a new randomly generated Self Signed Certificate during Runtime.
 
 Run `src/main/java/com/tw/arcast/secure/RxNettyHttpsSecureSslEngineServer.java` for Secured connection using the `secure(SslEngine)` approach.
 
 
  ## Working Secured Connection using secure(SslCodec) approach
  Run `src/main/java/com/tw/arcast/secure/RxNettyHttpsSecureSignedSslCodecServer.java` for Secured connection using the `secure(SslCodec)` approach - Fetches the `certificate.crt` and `privateKey.key` from `resources`.
 
  Run `src/main/java/com/tw/arcast/secure/RxNettyHttpsSecureSslCodecServer.java` for Secured connection using the `secure(SslCodec)` approach - Generates a new Self Signed Certificate on Runtime.
