package com.tw.arcast.server;

import com.tw.arcast.ExamplesEnvironment;
import io.reactivex.netty.protocol.http.server.HttpServer;
import rx.Observable;

public final class RxNettyHttpServer {

    public static void main(final String[] args) {

        ExamplesEnvironment env = ExamplesEnvironment.newEnvironment(RxNettyHttpServer.class);

        HttpServer server = HttpServer.newServer()
                .start((req, resp) ->
                        resp.writeString(Observable.just("Hello World!"))
                );
        System.out.println("Server Started at: http://localhost:" + server.getServerPort());

        /*Wait for shutdown if not called from the client (passed an arg)*/
        if (env.shouldWaitForShutdown(args)) {
            server.awaitShutdown();
        }

        /*If not waiting for shutdown, assign the ephemeral port used to a field so that it can be read and used by
        the caller, if any.*/
        env.registerServerAddress(server.getServerAddress());
    }
}
