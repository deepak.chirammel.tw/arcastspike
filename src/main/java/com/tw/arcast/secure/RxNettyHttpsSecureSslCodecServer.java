package com.tw.arcast.secure;

import com.tw.arcast.ExamplesEnvironment;
import io.netty.buffer.ByteBufAllocator;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.SelfSignedCertificate;
import io.reactivex.netty.protocol.http.server.HttpServer;
import io.reactivex.netty.ssl.DefaultSslCodec;
import io.reactivex.netty.ssl.SslCodec;
import rx.Observable;
import rx.exceptions.Exceptions;
import rx.functions.Func1;

import javax.net.ssl.SSLEngine;

public final class RxNettyHttpsSecureSslCodecServer {

    public static void main(final String[] args) {

        ExamplesEnvironment env = ExamplesEnvironment.newEnvironment(RxNettyHttpsSecureSslCodecServer.class);

        SslCodec sslCodec = getSslCodec();
        HttpServer server = HttpServer.newServer();

        if (sslCodec != null) {
            server.secure(sslCodec);
        }

        server.start((req, resp) -> resp.writeString(Observable.just("Hello World!")));
        System.out.println("Server Started at: https://localhost:" + server.getServerPort());

        /*Wait for shutdown if not called from the client (passed an arg)*/
        if (env.shouldWaitForShutdown(args)) {
            server.awaitShutdown();
        }

        /*If not waiting for shutdown, assign the ephemeral port used to a field so that it can be read and used by
        the caller, if any.*/
        env.registerServerAddress(server.getServerAddress());
    }

    private static SslCodec getSslCodec() {
        return new DefaultSslCodec(new Func1<ByteBufAllocator, SSLEngine>() {
            private SelfSignedCertificate ssc;
            @Override
            public SSLEngine call(ByteBufAllocator allocator) {
                try {
                    if (ssc == null) {
                        ssc = new SelfSignedCertificate();
                    }
                    return SslContextBuilder.forServer(ssc.certificate(), ssc.privateKey())
                            .build()
                            .newEngine(allocator);
                } catch (Exception e) {
                    throw Exceptions.propagate(e);
                }
            }
        });
    }
}
