package com.tw.arcast.secure;

import com.tw.arcast.ExamplesEnvironment;
import io.netty.buffer.ByteBufAllocator;
import io.netty.handler.ssl.SslContextBuilder;
import io.reactivex.netty.protocol.http.server.HttpServer;
import io.reactivex.netty.ssl.DefaultSslCodec;
import io.reactivex.netty.ssl.SslCodec;
import rx.Observable;
import rx.exceptions.Exceptions;
import rx.functions.Func1;

import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLException;
import java.io.File;

public final class RxNettyHttpsSecureSignedSslCodecServer {

    public static void main(final String[] args) {

        ExamplesEnvironment env = ExamplesEnvironment.newEnvironment(RxNettyHttpsSecureSignedSslCodecServer.class);

        HttpServer server = HttpServer.newServer();

        SslCodec sslCodec = getSslCodec();
        if (sslCodec != null) {
            server.secure(sslCodec);
        }

        server.start((req, resp) -> resp.writeString(Observable.just("Hello World!")));
        System.out.println("Server Started at: https://localhost:" + server.getServerPort());

        /*Wait for shutdown if not called from the client (passed an arg)*/
        if (env.shouldWaitForShutdown(args)) {
            server.awaitShutdown();
        }

        /*If not waiting for shutdown, assign the ephemeral port used to a field so that it can be read and used by
        the caller, if any.*/
        env.registerServerAddress(server.getServerAddress());
    }

    private static SslCodec getSslCodec() {
        final ClassLoader classLoader = RxNettyHttpsSecureSignedSslCodecServer.class.getClassLoader();
        File certificate = new File(classLoader.getResource("certificate.crt").getFile());
        File privateKey = new File(classLoader.getResource("privateKey.key").getFile());
        return new DefaultSslCodec(new Func1<ByteBufAllocator, SSLEngine>() {
            @Override
            public SSLEngine call(ByteBufAllocator allocator) {
                try {
                    return SslContextBuilder.forServer(certificate, privateKey)
                            .build()
                            .newEngine(allocator);
                } catch (SSLException exception) {
                    throw Exceptions.propagate(exception);
                }
            }
        });
    }
}
