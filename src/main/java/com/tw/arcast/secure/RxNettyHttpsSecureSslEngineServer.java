package com.tw.arcast.secure;

import com.tw.arcast.ExamplesEnvironment;
import io.reactivex.netty.protocol.http.server.HttpServer;
import rx.Observable;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.TrustManagerFactory;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

public class RxNettyHttpsSecureSslEngineServer {

    public static void main(final String[] args) {

        ExamplesEnvironment env = ExamplesEnvironment.newEnvironment(RxNettyHttpsSecureSslEngineServer.class);

        SSLEngine sslEngine = CreateSslEngine();
        HttpServer server = HttpServer.newServer();

        if (sslEngine != null) {
            server.secure(sslEngine);
        }

        server.start((req, resp) -> resp.writeString(Observable.just("Hello World!")));
        System.out.println("Server Started at: https://localhost:" + server.getServerPort());

        /*Wait for shutdown if not called from the client (passed an arg)*/
        if (env.shouldWaitForShutdown(args)) {
            server.awaitShutdown();
        }

        /*If not waiting for shutdown, assign the ephemeral port used to a field so that it can be read and used by
        the caller, if any.*/
        env.registerServerAddress(server.getServerAddress());
    }

    private static SSLEngine CreateSslEngine() {
        try {
            // Initialise the keystore
            char[] password = "simulator".toCharArray();
            KeyStore keyStore = KeyStore.getInstance("JKS");
            InputStream inputStream = RxNettyHttpsSecureSslEngineServer.class.getClassLoader().getResourceAsStream("jks.keystore");
            keyStore.load(inputStream, password);

            // Set up the key manager factory
            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("SunX509");
            keyManagerFactory.init(keyStore, password);

            // Set up the trust manager factory
            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance("SunX509");
            trustManagerFactory.init(keyStore);

            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(keyManagerFactory.getKeyManagers(), trustManagerFactory.getTrustManagers(), null);
            return sslContext.createSSLEngine();
        } catch (KeyStoreException exception) {
            System.out.println("KeyStoreException:");
            exception.printStackTrace();
        } catch (CertificateException exception) {
            System.out.println("CertificateException:");
            exception.printStackTrace();
        } catch (NoSuchAlgorithmException exception) {
            System.out.println("NoSuchAlgorithmException:");
            exception.printStackTrace();
        } catch (IOException exception) {
            System.out.println("IOException:");
            exception.printStackTrace();
        } catch (UnrecoverableKeyException exception) {
            System.out.println("UnrecoverableKeyException:");
            exception.printStackTrace();
        } catch (KeyManagementException exception) {
            System.out.println("KeyManagementException:");
            exception.printStackTrace();
        }
        System.out.println("SSLEngine is null!!!");
        return null;
    }
}
